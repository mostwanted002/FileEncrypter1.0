package operators;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

public class encrypter implements Runnable {

    private String dir;
    private String fileName;
    private char[] password;

    public encrypter(String directory, String file, char[] pass) {

        dir = directory;
        fileName = file;
        password = pass;
        Thread t = new Thread(this, "Encryption Thread");
        t.start();
    }
    private void encryptFile() throws Exception {

        FileInputStream fileRead = new FileInputStream(dir+fileName);
        FileOutputStream fileWrite = new FileOutputStream(dir+fileName.substring(0,fileName.lastIndexOf('.'))+getExtension());
        byte [] data = new byte [64];
        byte [] eData;
        byte [] iv = {0, 1, 0, 0, 6, 0, 4, 0, 5, 4, 0, 0, 0, 7, 7, 7};
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        Cipher C = Cipher.getInstance("AES/CBC/NoPadding");
        C.init(Cipher.ENCRYPT_MODE, getSecretKey(), ivSpec);
        //CipherInputStream eStream = new CipherInputStream(fileRead, C);

        while (fileRead.read(data) != -1) {
            eData = C.doFinal(data);
            fileWrite.write(eData);
        }
        fileRead.close();
        fileWrite.close();
    }

    private SecretKey getSecretKey() throws Exception {

        FileInputStream ksRead = new FileInputStream("./.Encrypto/key.ks");
        KeyStore ks = KeyStore.getInstance("JCEKS");
        ks.load(ksRead,password);
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
        KeyStore.SecretKeyEntry skEntry = (KeyStore.SecretKeyEntry) ks.getEntry("eKey",protParam);
        return skEntry.getSecretKey();
    }
    static SecretKey getSecretKey(char[] password) throws KeyStoreException, IOException, NoSuchAlgorithmException,
            CertificateException, UnrecoverableEntryException {

        System.out.println("SecretKey needed for decrypter");
        FileInputStream ksRead = new FileInputStream("./.Encrypto/key.ks");
        KeyStore ks = KeyStore.getInstance("JCEKS");
        ks.load(ksRead, password);
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
        KeyStore.SecretKeyEntry skEntry = (KeyStore.SecretKeyEntry) ks.getEntry("eKey",protParam);
        return skEntry.getSecretKey();
    }

    public void run() {
        try{
            encryptFile();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    //Function to get provided file's extension
    private String getExtension(){
        return "_encrypted"+fileName.substring(fileName.lastIndexOf('.'));
    }
    static String getExtension(String fileName){
        return "decrypted"+fileName.substring(fileName.lastIndexOf('.'));
    }
}
