package operators;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static operators.encrypter.getSecretKey;
import static operators.encrypter.getExtension;

public class decrypter implements Runnable {
    public static final File configDir = new File("./.Encrypto/");
    private static String passwordFromFile;
    private static final String suffix = "fiLeeNcrypto";
    private String dir;
    private String fileName;
    private char[] password;

    public decrypter(String directory, String file, char[] pass) {
        dir = directory;
        fileName = file;
        password = pass;
        Thread t = new Thread(this, "Decryption Thread");
        t.start();
    }

    //For New Login
    public static void newLogin(char[] password) throws Exception {

        boolean successfulReadOnly;
        String hashedValue = hasher(Arrays.toString(password) + suffix);
        FileOutputStream storeKey = new FileOutputStream("./.Encrypto/dekry.pt");
        storeKey.write(hashedValue.getBytes());
        storeKey.close();
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        keygen.init(256);
        SecretKey keychain = keygen.generateKey();
        try {
            saveKey(keychain, password);
        } catch (Exception error) {

            System.out.println(error.toString());

        } finally {
            System.out.println("new-login");
        }
        successfulReadOnly = configDir.setReadOnly();
        if(!successfulReadOnly){
            System.out.println("Can't set Read-Only Attribute");
        }
    }
    //Login for ExistingUser
    public static boolean isLoginSuccess(char[] password) throws IOException {

        FileInputStream fromStoredKey = new FileInputStream("./.Encrypto/dekry.pt");
        BufferedReader getKey = new BufferedReader(new InputStreamReader(fromStoredKey));
        String fetchedPass;

        while ((fetchedPass = getKey.readLine()) != null) {

            passwordFromFile = fetchedPass;
        }
        getKey.close();
        String enteredPass = hasher(Arrays.toString(password) + suffix);
        return enteredPass.equals(passwordFromFile);
    }

    //Used to hash Password
    private static String hasher(String password) {
        StringBuilder hashedValue = new StringBuilder();

        try {

            MessageDigest sha = MessageDigest.getInstance("SHA-512");
            byte[] hashedBytes = sha.digest(password.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f'};
            for (byte b : hashedBytes) {

                hashedValue.append(digits[(b & 0xf0) >> 12]);
                hashedValue.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException error) {

            System.out.println("error in hasher.");
        }
        return hashedValue.toString();
    }

    private static void saveKey(SecretKey userkey, char[] password) throws Exception {

        FileOutputStream oout = new FileOutputStream("./.Encrypto/key.ks");
        KeyStore ks = KeyStore.getInstance("JCEKS");
        ks.load(null, password);
        KeyStore.SecretKeyEntry keyEntry = new KeyStore.SecretKeyEntry(userkey);
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
        ks.setEntry("eKey", keyEntry, protParam);
        ks.store(oout, password);
        oout.close();
    }

    private void decryptFile() throws Exception {

        FileInputStream fileRead = new FileInputStream(dir + fileName);
        FileOutputStream fileWrite = new FileOutputStream(dir + fileName.substring(0,fileName.indexOf("encrypted"))+getExtension(fileName));
        byte[] data = new byte[64];
        byte[] dData;// = new byte [(dir+fileName).length()];
        byte[] iv = {0, 1, 0, 0, 6, 0, 4, 0, 5, 4, 0, 0, 0, 7, 7, 7};
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        Cipher C = Cipher.getInstance("AES/CBC/NoPadding");
        C.init(Cipher.DECRYPT_MODE, readProgramKey(), ivSpec);
        //CipherInputStream dStream = new CipherInputStream(fileRead, C);
        while (fileRead.read(data) != -1) {
            dData = C.doFinal(data);
            fileWrite.write(dData);

        }

        //dStream.close();
        fileRead.close();
        fileWrite.close();
    }

    private SecretKey readProgramKey() throws Exception {

        return getSecretKey(password);
    }


    public void run() {
        try {
            decryptFile();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
