import operators.decrypter;
import operators.encrypter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class MainExec extends JApplet implements ActionListener {
    private MainExec() {

        init();
    }

    private boolean isSuccess = true;
    private static File configDir = decrypter.configDir;
    private File key = new File (configDir,"dekry.pt");
    private boolean isNotFirstRun = configDir.exists();
    private boolean isExistingUser = key.exists();
    private JFrame Login;
    private JPanel UI = new JPanel();
    private JButton encryptFile = new JButton("Encrypt File");
    private JButton decryptFile = new JButton("Decrypt File");
    private JButton submitPass = new JButton("Login");
    private JPasswordField password1 = new JPasswordField();
    private JPasswordField passwordVerify = new JPasswordField();
    private JLabel mainDisplay = new JLabel();
    private JLabel enterPassword = new JLabel("Enter Password: ");
    private JLabel verifyPassword = new JLabel("Verify Password: ");

    //Initialising Frame
    public void init() {
        Login = new JFrame("Login Window");
        Login.setSize(1024, 768);
        UI.setBounds(0,0,1024, 748);
        password1.setBounds(437,200,150,30);
        password1.setBackground(Color.BLACK);
        password1.setForeground(Color.GREEN);
        passwordVerify.setBounds(437,240,150,30);
        passwordVerify.setBackground(Color.BLACK);
        passwordVerify.setForeground(Color.GREEN);
        enterPassword.setBounds(307, 200, 130, 30);
        enterPassword.setHorizontalAlignment(JLabel.CENTER);
        verifyPassword.setBounds(307, 240, 130, 30);
        verifyPassword.setHorizontalAlignment(JLabel.CENTER);
        mainDisplay.setBounds(432, 650, 160,30);
        mainDisplay.setText("Enter Password");
        mainDisplay.setHorizontalAlignment(JLabel.CENTER);
        Login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        submitPass.setBounds(462, 425, 100,30);
        submitPass.addActionListener(this);
        encryptFile.addActionListener(this);
        decryptFile.addActionListener(this);

        if (isNotFirstRun&&!isExistingUser) {
            mainDisplay.setText("KEY FILE DELETED/TAMPERED!");
            mainDisplay.setBounds(542, 350, 300,30);
        }
            UI.add(submitPass);
            UI.add(enterPassword);
            UI.add(password1);
            UI.add(mainDisplay);

            if (!isExistingUser) {
                UI.add(passwordVerify);
                UI.add(verifyPassword);
            }

        UI.setLayout(null);
        Login.add(UI);
        Login.setLayout(null);
        Login.setVisible(true);
    }
    //Login/Encrypt/Decrypt
    public void actionPerformed(ActionEvent ae) {
        String str = ae.getActionCommand();
        FileDialog fileSelection;
        switch(str) {
            case "Login":
            {
                if ((!isNotFirstRun)&&(!isExistingUser) && (Arrays.equals(password1.getPassword(), passwordVerify.getPassword())))
                {
                    try
                    {
                        isNotFirstRun = configDir.mkdir();
                        decrypter.newLogin(password1.getPassword());
                    }catch(Exception error)
                    {
                        System.out.println("Error in creating file!");
                        mainDisplay.setText("AN ERROR HAS OCCURED!");
                        UI.add(mainDisplay);
                        UI.updateUI();
                    }
                    System.out.println("New Login Done");
                    UI.removeAll();
                    encryptFile.setBounds(200, 200, 150, 50);
                    decryptFile.setBounds(800, 200, 150, 50);
                    UI.add(encryptFile); UI.add(decryptFile);
                    UI.updateUI();
                }
                else if (!isExistingUser && (password1.getPassword() != passwordVerify.getPassword()))
                {
                    mainDisplay.setText("Passwords Don't Match! Try Again.");
                    mainDisplay.setBounds(342, 650, 300,30);
                    UI.add(mainDisplay);
                    UI.updateUI();
                }

                else if (isNotFirstRun&&isExistingUser)
                {
                    try {
                        if (!decrypter.isLoginSuccess(password1.getPassword()))
                        {
                            mainDisplay.setText("Incorrect Password. TRY AGAIN!");
                            UI.updateUI();
                            System.out.println("IncorrectPass");
                            break;
                        }
                    }catch (IOException error){
                        System.out.println("Error in accessing file!");
                        mainDisplay.setText("AN ERROR HAS OCCURRED!");
                    }
                    UI.removeAll();
                    encryptFile.setBounds(200, 200, 150, 50);
                    decryptFile.setBounds(800, 200, 150, 50);
                    UI.add(encryptFile); UI.add(decryptFile);
                    UI.updateUI();
                }
            }
            break;
            case "Encrypt File":
                {
                    fileSelection = new FileDialog(Login, "Select File for Encryption");
                    fileSelection.setVisible(true);
                    fileSelection.setFocusable(true);

                    try{
                        new encrypter(fileSelection.getDirectory(), fileSelection.getFile(),
                                password1.getPassword());
                    } catch (Exception e){
                        System.out.println("Error in encrypting file");
                        System.out.println(e.getMessage());
                        isSuccess = false;
                    }
                    if(isSuccess)
                    {
                        mainDisplay.setText("Encryption Success");
                        System.out.println("Encrypted the provided file.");
                    }
                    else mainDisplay.setText("Error while encrypting. Try Again.");
                    encryptFile.setBounds(200, 200, 150, 50);
                    decryptFile.setBounds(800, 200, 150, 50);
                    mainDisplay.setBounds(342, 650, 300,30);
                    UI.add(mainDisplay);UI.add(encryptFile); UI.add(decryptFile);
                    UI.updateUI();
                    }
            break;
            case "Decrypt File":
                {
                    fileSelection = new FileDialog(Login, "Select File for Decryption");
                    fileSelection.setVisible(true);
                    fileSelection.setFocusable(true);
                    try{
                        new decrypter(fileSelection.getDirectory(),
                                fileSelection.getFile(), password1.getPassword());
                    } catch (Exception e){
                        System.out.println("Error in decrypting file"); isSuccess = false;
                        System.out.println(Arrays.toString(e.getStackTrace()));
                    }
                    if(isSuccess)
                    {
                        mainDisplay.setText("Decryption Success");
                        System.out.println("Decrypted the provided file.");
                    }
                    else mainDisplay.setText("Error while decrypting. Try Again.");
                    encryptFile.setBounds(200, 200, 150, 50);
                    decryptFile.setBounds(800, 200, 150, 50);
                    mainDisplay.setBounds(342, 650, 300,30);
                    UI.add(mainDisplay); UI.add(encryptFile); UI.add(decryptFile);
                    UI.updateUI();
                }
        }
    }

    public static void main(String args[])
    {
        //An optimized way
        SwingUtilities.invokeLater(MainExec::new);
    }
}
